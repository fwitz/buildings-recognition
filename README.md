# Research project: Aerial images buildings recognition using deep learning

This project was made in the context of my last semester at UTBM as a research project. The aim was to developp a 
working deep learning network to detect buildings on images taken by drone and, in the end, generate a obj file for further simulation in drone control in unity.

I was working with three other mates working on other parts of this project: moving object detection and automation for drone landing.

In this repo you'll find a python notebook with all my work, some useful python scripts I wrote for the project, the trained models files, the dataset and the final article on the project.

